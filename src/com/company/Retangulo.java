package com.company;

import java.util.ArrayList;

public class Retangulo extends FormaGeometrica {
    double areaRetangulo;

    public Retangulo(ArrayList<Integer> tamanhoLado) {
        super(tamanhoLado);
    }

    public void calcularRetangulo(){
        areaRetangulo = tamanhoLado.get(0) * tamanhoLado.get(1);
        System.out.println("A Area do Retangulo é: " + areaRetangulo);
    }
}
