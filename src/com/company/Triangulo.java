package com.company;

import java.util.ArrayList;

public class Triangulo extends FormaGeometrica {
    boolean ehTriangulo;
    double areaTriangulo;

    public Triangulo(ArrayList<Integer> tamanhoLado) {
        super(tamanhoLado);
    }

    public void validarTriangulo() {
        if ((tamanhoLado.get(0) + tamanhoLado.get(1)) > tamanhoLado.get(2) &&
            (tamanhoLado.get(0) + tamanhoLado.get(2)) > tamanhoLado.get(1) &&
            (tamanhoLado.get(1) + tamanhoLado.get(2)) > tamanhoLado.get(0)) {
            ehTriangulo = true;
        } else {
            System.out.println("Medidas Invalidas para um Triangulo");
            ehTriangulo = false;
        }
    }

    public void calcularTriangulo(){
        if (ehTriangulo) {
            double s = (tamanhoLado.get(1) + tamanhoLado.get(2) + tamanhoLado.get(3)) / 2;
            areaTriangulo = Math.sqrt(s * (s - tamanhoLado.get(1)) * (s - tamanhoLado.get(2)) * (s - tamanhoLado.get(3)));
            System.out.println("A area do Triangulo é: " + areaTriangulo);
        }
    }
}
