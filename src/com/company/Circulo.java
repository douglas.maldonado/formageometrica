package com.company;

import java.util.ArrayList;

public class Circulo extends FormaGeometrica {
    double areaCirculo;

    public Circulo(ArrayList<Integer> tamanhoLado) {
        super(tamanhoLado);
    }

    public void calcularCirculo(){
        System.out.println(tamanhoLado);
        areaCirculo = Math.pow(tamanhoLado.get(0), 2) * Math.PI;
        System.out.println("A Area do Circulo é: " + areaCirculo);
    }
}
