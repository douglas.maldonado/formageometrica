package com.company;

import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

//Pensar em transformar formaGeometrica em Abstract

public class FormaGeometrica {
    ArrayList<Integer> tamanhoLado = new ArrayList<>();
    int forma;

    public FormaGeometrica(ArrayList<Integer> tamanhoLado) {
        this.tamanhoLado = tamanhoLado;
    }

    public FormaGeometrica() {
    }

    public ArrayList<Integer> getTamanhoLado() {
        return tamanhoLado;
    }

//    public void definirForma(){
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Selecione a Forma para Calcular");
//        System.out.println("Circulo = 1 - Retangulo = 2 - Triangulo = 3");
//        forma = scanner.nextInt();
//    }

    public void informarLados(){
        Scanner scanner = new Scanner(System.in);
        String controle = "S";

        do{
            if (tamanhoLado.size() < 3){

                System.out.println("Digite o Tamanho do Lado da Figura Geometrica");
                tamanhoLado.add(scanner.nextInt());
                System.out.println("Deseja Continuar? [S/N]: ");
                controle = scanner.next();

            } else {

                System.out.println("Nao é permitido mais de 3 lados");
                controle = "N";

            }
        } while (!controle.equals("N"));
    }

    public void tipoForma(){
        Circulo circulo = new Circulo(tamanhoLado);
        Triangulo triangulo = new Triangulo(tamanhoLado);
        Retangulo retangulo = new Retangulo(tamanhoLado);
        if (tamanhoLado.size() == 1){
           System.out.println("Calcular Circulo");
           circulo.calcularCirculo();
        } else if (tamanhoLado.size() == 2) {
            System.out.println("Calcular Retangulo");
            retangulo.calcularRetangulo();
        } else {
            System.out.println("Calcular Triangulo");
            triangulo.validarTriangulo();
            triangulo.calcularTriangulo();
        }
    }
}
